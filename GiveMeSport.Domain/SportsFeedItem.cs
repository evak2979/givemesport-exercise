﻿using System;

namespace GiveMeSport.Domain
{
    public class SportsFeedItem
    {
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public string Summary { get; set; }
        public string Title { get; set; }
        public Uri Uri { get; set; }
    }
}
