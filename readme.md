Progress milestones so far:

Have mapped the feed to a domain object of our own
Created the builder pattern to construct this domain on a business layer (that'll make it available for both our web and our api front ends)
Added canary test to make sure the givemesports endpoint is active
Added unit tests against all implementations
Hooked up simple injector to handle dependency injection (I was running out of time to sleep, so for now I have 'cheated' by referencing both business and data from web. Ideally only business should be referenced from web, and data from business, to keep the layers separate. I could (or will) either try to create class inheritance between the layers to register sequentially, or an extra project overall to handle container registrations - provided you want me to!)

Next step:

MVC 5 front end, + jquery (or aurelia) auto updates of latest news.

Point:
I noticed the MVC 4 requirement all too late... Sorry, I am too used to working with MVC 5 by now. Is this okay to keep working on or would you rather I downgrade the project?
Thanks !