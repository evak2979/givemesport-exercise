﻿using System.Collections.Generic;
using System.Linq;
using GiveMeSport.Data.Interfaces;
using SimpleFeedReader;

namespace GiveMeSport.Data.Concretes
{
    public class SimpleFeedReaderWrapper : ISimpleFeedReaderWrapper
    {
        public IEnumerable<FeedItem> RetrieveFeed(string url)
        {
            var reader = new FeedReader();
            IEnumerable<FeedItem> items = reader.RetrieveFeed(url);

            return items;
        }
    }
}
