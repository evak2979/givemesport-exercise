﻿using System;
using System.Collections.Generic;
using GiveMeSport.Data.Interfaces;
using GiveMeSport.Domain;
using SimpleFeedReader;

namespace GiveMeSport.Data.Concretes
{
    public class FeedMapper : IFeedMapper
    {
        public FeedMapper()
        {
            
        }

        public IEnumerable<SportsFeedItem> GetMappedFeedItems(IEnumerable<FeedItem> items)
        {
            var listOfSportsFeedItems = new List<SportsFeedItem>();

            if (items != null)
            {
                foreach (var feedItem in items)
                {
                    listOfSportsFeedItems.Add(Map(feedItem));
                }
            }

            return listOfSportsFeedItems;
        }

        public SportsFeedItem Map(FeedItem item)
        {
            var sportsFeedItem = new SportsFeedItem
            {
                Content = item.Content,
                Date = item.Date.DateTime,
                Summary = item.Summary,
                Title = item.Title,
                Uri = item.Uri
            };

            return sportsFeedItem;
        }
    }
}
