﻿using System.Collections.Generic;
using GiveMeSport.Domain;
using SimpleFeedReader;

namespace GiveMeSport.Data.Interfaces
{
    public interface IFeedMapper
    {
        IEnumerable<SportsFeedItem> GetMappedFeedItems(IEnumerable<FeedItem> items);
    }
}