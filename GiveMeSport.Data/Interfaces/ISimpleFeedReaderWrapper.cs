﻿using System.Collections.Generic;
using SimpleFeedReader;

namespace GiveMeSport.Data.Interfaces
{
    public interface ISimpleFeedReaderWrapper
    {
        IEnumerable<FeedItem> RetrieveFeed(string url);
    }
}
