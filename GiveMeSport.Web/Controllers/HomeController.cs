﻿using System.Web.Mvc;
using GiveMeSport.Business.Interfaces;
using Newtonsoft.Json;

namespace GiveMeSport.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISportsFeedItemDirector _director;

        public HomeController(ISportsFeedItemDirector director)
        {
            _director = director;
        }

        public JsonResult Index()
        {
            return Json(_director.Construct(), JsonRequestBehavior.AllowGet);
        }
    }
}