using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using GiveMeSport.Business.Interfaces;
using GiveMeSport.Data.Interfaces;
using GiveMeSport.Web;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;

[assembly: WebActivator.PostApplicationStartMethod(typeof(SimpleInjectorInitializerWeb), "Initialize")]

namespace GiveMeSport.Web
{
    public static class SimpleInjectorInitializerWeb
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            
            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {
            // For instance:
            // container.Register<IUserRepository, SqlUserRepository>(Lifestyle.Scoped);

            RegisterBusinessDependencies(container);
            RegisterDataDependencies(container);
        }

        private static void RegisterBusinessDependencies(Container container)
        {
            var businessAssembly = typeof(ISportsFeedItemBuilder).Assembly;

            var businessRegistrations =
                from type in businessAssembly.GetExportedTypes()
                where type.GetInterfaces().Any()
                select new {Service = type.GetInterfaces().Single(), Implementation = type};


            foreach (var reg in businessRegistrations)
            {
                container.Register(reg.Service, reg.Implementation, Lifestyle.Transient);
            }
        }

        private static void RegisterDataDependencies(Container container)
        {
            var dataAssembly = typeof(ISimpleFeedReaderWrapper).Assembly;

            var dataRegistrations =
                from type in dataAssembly.GetExportedTypes()
                where type.GetInterfaces().Any()
                select new {Service = type.GetInterfaces().Single(), Implementation = type};


            foreach (var reg in dataRegistrations)
            {
                container.Register(reg.Service, reg.Implementation, Lifestyle.Transient);
            }
        }
    }
}