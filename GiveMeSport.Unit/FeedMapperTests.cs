﻿using System;
using System.Collections.Generic;
using System.Linq;
using GiveMeSport.Data.Concretes;
using GiveMeSport.Domain;
using NUnit.Framework;
using SimpleFeedReader;

namespace GiveMeSport.Unit
{
    [TestFixture]
    public class FeedMapperTests
    {
        private FeedMapper _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new FeedMapper();
        }

        [TestCaseSource("GetFeedItems")]
        public void should_return_a_non_null_list_of_T_items_if_feedItems_are_null_or_empty(IEnumerable<FeedItem> feedItems)
        {
            //when
            IEnumerable<SportsFeedItem> result = _sut.GetMappedFeedItems(feedItems);

            //then
            Assert.IsNotNull(result);
        }

        [Test]
        public void should_return_a_properly_populated_list_of_T_items()
        {
            //given
            IEnumerable<FeedItem> feedItems = new List<FeedItem>()
            {
                new FeedItem
                {
                    Content = "Content",
                    Date = new DateTime(2000, 1, 1),
                    Summary = "Summary",
                    Title = "Title",
                    Uri = new Uri("http://www.uri.com")
                }
            };

            //when
            IEnumerable<SportsFeedItem> result = _sut.GetMappedFeedItems(feedItems);

            //then
            Assert.That(result.First().Content, Is.EqualTo("Content"));
            Assert.That(result.First().Date, Is.EqualTo(new DateTime(2000, 1, 1)));
            Assert.That(result.First().Summary, Is.EqualTo("Summary"));
            Assert.That(result.First().Title, Is.EqualTo("Title"));
            Assert.That(result.First().Uri, Is.EqualTo(feedItems.First().Uri));
        }

        private static IEnumerable<IEnumerable<FeedItem>> GetFeedItems
        {
            get
            {
                yield return null;
                yield return new FeedItem[0];
            }
        }
    }
}
