﻿using System.Collections.Generic;
using GiveMeSport.Business.Concretes;
using GiveMeSport.Business.Interfaces;
using GiveMeSport.Domain;
using Moq;
using NUnit.Framework;

namespace GiveMeSport.Unit
{
    [TestFixture]
    public class RssSportsFeedItemDirectorTests
    {
        private RssSportsFeedItemDirector _sut;
        private Mock<ISportsFeedItemBuilder> _mockBuilder;

        [SetUp]
        public void Setup()
        {
            _mockBuilder = new Mock<ISportsFeedItemBuilder>();
            _sut = new RssSportsFeedItemDirector(_mockBuilder.Object);
        }

        [Test]
        public void should_call_the_builder_ReadFromEndpoint_function_with_correct_url()
        {
            //when
            _sut.Construct();

            //then
            _mockBuilder.Verify(x=>x.ReadFromEndpoint("http://www.givemesport.com/rss.ashx"));
        }

        [Test]
        public void should_call_the_builder_MapToSportsFeedItems_function()
        {
            //when
            _sut.Construct();

            //then
            _mockBuilder.Verify(x => x.MapToSportsFeedItems());
        }

        [Test]
        public void should_call_the_builder_build_function()
        {
            //when
            _sut.Construct();

            //then
            _mockBuilder.Verify(x => x.Get());
        }

        [Test]
        public void should_return_a_correct_list_of_items()
        {
            //given
            List<SportsFeedItem> items = new List<SportsFeedItem>();
            _mockBuilder.Setup(x => x.Get()).Returns(items);

            //when
            IEnumerable<SportsFeedItem> results = _sut.Construct();

            //then
            Assert.That(results, Is.EqualTo(items));
        }
    }
}
