﻿using System;
using System.Collections.Generic;
using System.Linq;
using GiveMeSport.Business.Concretes;
using GiveMeSport.Data.Interfaces;
using GiveMeSport.Domain;
using Moq;
using NUnit.Framework;
using SimpleFeedReader;

namespace GiveMeSport.Unit
{
    [TestFixture]
    public class SportsFeedItemBuilderTests
    {
        private Mock<ISimpleFeedReaderWrapper> _mockWrapper;
        private Mock<IFeedMapper> _mockMapper;
        private SportsFeedItemBuilder _sut;

        [SetUp]
        public void Setup()
        {
            _mockWrapper = new Mock<ISimpleFeedReaderWrapper>();
            _mockMapper = new Mock<IFeedMapper>();

            _sut = new SportsFeedItemBuilder(_mockWrapper.Object, _mockMapper.Object);
        }

        [Test]
        public void should_call_the_feed_the_endpoint_with_the_correct_url()
        {
            //given
            string url = "someurl";

            //when
            _sut.ReadFromEndpoint(url);

            //then
            _mockWrapper.Verify(x => x.RetrieveFeed(url));
        }

        [Test]
        public void should_call_the_feedmapper_when_mapping_feeditems_to_sportfeeditems()
        {
            //given
            string url = "someurl";
            List<FeedItem> feedItems = new List<FeedItem>();
            _mockWrapper.Setup(x => x.RetrieveFeed(url)).Returns(feedItems);

            //when
            _sut.ReadFromEndpoint(url);
            _sut.MapToSportsFeedItems();

            //then
            _mockMapper.Verify(x => x.GetMappedFeedItems(feedItems));
        }

        [Test]
        public void should_return_properly_mapped_items_when_calling_get()
        {
            //given
            string url = "someurl";
            Uri uri = new Uri("http://www.uri.com");
            List <FeedItem> feedItems = new List<FeedItem>();
            _mockWrapper.Setup(x => x.RetrieveFeed(url)).Returns(feedItems);
            _mockMapper.Setup(x => x.GetMappedFeedItems(feedItems)).Returns(
                new List<SportsFeedItem>
                {
                    new SportsFeedItem()
                    {
                        Content = "Content",
                        Date = new DateTime(2000, 1, 1),
                        Summary = "Summary",
                        Title = "Title",
                        Uri = uri
                    }
                });

            //when
            _sut.ReadFromEndpoint(url);
            _sut.MapToSportsFeedItems();
            IEnumerable<SportsFeedItem> items = _sut.Get();

            //then
            Assert.That(items.Count(), Is.EqualTo(1));
            Assert.That(items.First().Content, Is.EqualTo("Content"));
            Assert.That(items.First().Date, Is.EqualTo(new DateTime(2000, 1, 1)));
            Assert.That(items.First().Summary, Is.EqualTo("Summary"));
            Assert.That(items.First().Title, Is.EqualTo("Title"));
            Assert.That(items.First().Uri, Is.EqualTo(uri));
        }
    } 
}
