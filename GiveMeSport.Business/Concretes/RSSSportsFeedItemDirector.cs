﻿using System.Collections.Generic;
using GiveMeSport.Business.Interfaces;
using GiveMeSport.Domain;

namespace GiveMeSport.Business.Concretes
{
    public class RssSportsFeedItemDirector : ISportsFeedItemDirector
    {
        private readonly ISportsFeedItemBuilder _builder;

        public RssSportsFeedItemDirector(ISportsFeedItemBuilder builder)
        {
            _builder = builder;
        }

        public IEnumerable<SportsFeedItem> Construct()
        {
            _builder.ReadFromEndpoint("http://www.givemesport.com/rss.ashx");
            _builder.MapToSportsFeedItems();

            return _builder.Get();
        }
    }
}
