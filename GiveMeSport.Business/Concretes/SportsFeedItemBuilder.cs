using System.Collections.Generic;
using GiveMeSport.Business.Interfaces;
using GiveMeSport.Data.Interfaces;
using GiveMeSport.Domain;
using SimpleFeedReader;

namespace GiveMeSport.Business.Concretes
{
    public class SportsFeedItemBuilder : ISportsFeedItemBuilder
    {
        private readonly ISimpleFeedReaderWrapper _feedReaderWrapper;
        private readonly IFeedMapper _feedMapper;
        private IEnumerable<SportsFeedItem> _mappedItems;
        private IEnumerable<FeedItem> _feedItems;

        public SportsFeedItemBuilder(ISimpleFeedReaderWrapper feedReaderWrapper,
            IFeedMapper feedMapper)
        {
            _feedReaderWrapper = feedReaderWrapper;
            _feedMapper = feedMapper;
        }

        public void ReadFromEndpoint(string url)
        {
            _feedItems = _feedReaderWrapper.RetrieveFeed(url);
        }

        public void MapToSportsFeedItems()
        {
            _mappedItems = _feedMapper.GetMappedFeedItems(_feedItems);
        }

        public IEnumerable<SportsFeedItem> Get()
        {
            return _mappedItems;
        }
    }
}
