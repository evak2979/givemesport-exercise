﻿using System.Collections;
using System.Collections.Generic;
using GiveMeSport.Domain;

namespace GiveMeSport.Business.Interfaces
{
    public interface ISportsFeedItemBuilder
    {
        void ReadFromEndpoint(string url);
        void MapToSportsFeedItems();
        IEnumerable<SportsFeedItem> Get();
    }
}
