﻿using System.Collections.Generic;
using GiveMeSport.Domain;

namespace GiveMeSport.Business.Interfaces
{
    public interface ISportsFeedItemDirector
    {
        IEnumerable<SportsFeedItem> Construct();
    }
}
