﻿using System.Net;
using NUnit.Framework;

namespace GiveMeSport.Canary
{
    [TestFixture]
    public class GiveMeSportsTests
    {
        [Test]
        public void should_get_an_OK_response_from_GiveMeSports_RSS_Site()
        {
            //given
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest
                                           .Create("http://www.givemesport.com/rss.ashx");

            //when
            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

            //then
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }
    }
}
